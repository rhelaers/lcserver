const http = require('http')  
const url=require('url')
var requestMod = require('request');
var syncrequest=require('sync-request')
const port = 3001

var connecties=null;

function getLCSync(x){
    var res = syncrequest('POST', 'http://localhost:3000/endpoint/personalisatie', {
       
        'headers': {
            'Content-Type': 'application/json'
        },        
        json: {
                "route": {
                    "from":x.from,
                    "to":x.to
                },
                "person":{
                    "id" : "user1",
                    "firstName" : "John",
                    "lastName" : "Doe",
                    "profileModules" : {
                        "personal" : {
                            "preferredLanguage" : "EN",
                            "sex" : "M",
                            "age" : 30,
                            "walkingspeed":"normal"
                        },
                        "transport" : {
                            "maxWalkingDistance" : 2000,
                            "transferTimeNeeded" : 300,
                            "routeSortingPreference" : "minimizeDuration",
                            "modeRanking" : [
                                {
                                  "transportMode" : "train",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "bus",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "metro",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "tram",
                                  "score" : 1 
                                },
                                {
                                  "transportMode" : "foot",
                                  "score" : 1 
                                }

                            ]

                        }
                    }
                }
            } 
        });
        
        return JSON.parse(res.getBody('utf8'));
}



const requestHandler = (request, response) => {  
  var queryData=url.parse(request.url,true);
  
  var date=queryData.query.departureTime;
  if("undefined" === typeof  date){
      //console.log(queryData);
      var x=getDepartureAndArrival(queryData.path);
      console.log(x.from + "--->" + x.to);
      connecties=null;
      date=new Date();
//      getLC(function(resultaat){
//        var json = JSON.stringify(resultaat);
//        response.writeHead(200, {"Content-Type": "application/json"});
//        response.end(json);
//    });
    connecties=getLCSync(x);
    console.log(connecties);
  }
  var huidigeDateTime=new Date(date);
  var roundMinutesDateTime=roundMinutes(huidigeDateTime);
  var previousDateTime=substractMinutes(roundMinutesDateTime,10);
  var nextDateTime=addMinutes(roundMinutesDateTime,10);
//
//      console.log("**********************************")      
//      console.log(huidigeDateTime);      
//      console.log(roundMinutesDateTime)      
//      console.log(previousDateTime)
//      console.log(nextDateTime);
//      console.log("**********************************")
//      
  
    

    var result=[];
    var i=0;
    for(i in connecties){
        var con=connecties[i];
        var DateTimeRounded=roundMinutes(new Date(con.departureTime))
        if((DateTimeRounded.getTime()+2*3600*1000)===roundMinutesDateTime.getTime()){
            result.push(con);
        }
    }
    
//    console.log("---------------------------------")
//    console.log(result)
//    console.log("*******************************")
       
   var json=null;
   json = getLCPage(result,encodeURIComponent(transformLCDate(new Date(roundMinutesDateTime).toISOString())),encodeURIComponent(transformLCDate( new Date(previousDateTime).toISOString())),encodeURIComponent(transformLCDate(new Date(nextDateTime).toISOString())));
     
    response.writeHead(200, {"Content-Type": "application/ld+json"});
    response.end(json);
       
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {  
  if (err) {
    return console.log('something bad happened', err)  }

  console.log(`server is listening on ${port}`)
})



function getLCPage(graph, id, previous,next){
    json = JSON.stringify({
   "@context":{
      "hydra":"http://www.w3.org/ns/hydra/core#",
      "property":{
         "@id":"hydra:property",
         "@type":"@vocab"
      },
      "required":"hydra:required",
      "Collection":"hydra:Collection",
      "member":{
         "@id":"hydra:member",
         "@type":"@id"
      },
      "search":"hydra:search",
      "PagedCollection":"hydra:PagedCollection",
      "nextPage":{
         "@id":"hydra:nextPage",
         "@type":"@id"
      },
      "previousPage":{
         "@id":"hydra:previousPage",
         "@type":"@id"
      },
      "TemplatedLink":"hydra:TemplatedLink",
      "IriTemplate":"hydra:IriTemplate",
      "template":"hydra:template",
      "mapping":"hydra:mapping",
      "IriTemplateMapping":"hydra:IriTemplateMapping",
      "variable":"hydra:variable",
      "lc":"http://semweb.mmlab.be/ns/linkedconnections#",
      "gtfs":"http://vocab.gtfs.org/terms#",
      "Connection":"http://semweb.mmlab.be/ns/linkedconnections#Connection",
      "arrivalTime":"lc:arrivalTime",
      "departureTime":"lc:departureTime",
      "arrivalStop":{
         "@type":"@id",
         "@id":"http://semweb.mmlab.be/ns/linkedconnections#arrivalStop"
      },
      "departureStop":{
         "@type":"@id",
         "@id":"http://semweb.mmlab.be/ns/linkedconnections#departureStop"
      },
      "trip":{
         "@type":"@id",
         "@id":"gtfs:trip"
      }
   },
   "@id":"http://localhost:3001/?departureTime="+id,
   "@type":"PagedCollection",
   "nextPage":"http://localhost:3001/?departureTime="+next,
   "previousPage":"http://localhost:3001/?departureTime="+previous,
   "search":{
      "@type":"IriTemplate",
      "template":"http://localhost:3001/{?departureTime}",
      "variableRepresentation":"BasicRepresentation",
      "mapping":{
         "@type":"IriTemplateMapping",
         "variable":"departureTime",
         "required":true,
         "property":"http://semweb.mmlab.be/ns/linkedconnections#departureTimeQuery"
      }
   },
   "@graph":graph
   
});

    return json;
}

function transformLCDate(datum){
    var regexdatum=/(.*T.*:.*)(:.*)/.exec(datum);

    return regexdatum[1];
}

function getDepartureAndArrival(pathName){
    var regex=/\/(.*)\/(.*)/.exec(pathName);
    var x={};
    x.from=regex[1];
    x.to=regex[2];
    return x;
}

function roundMinutes(date) {
    date.setMinutes(Math.floor(date.getMinutes()/10)*10);
    date.setSeconds(00);
    date.setMilliseconds(000);
    return date;
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}

function substractMinutes(date, minutes) {
    return new Date(date.getTime() - minutes*60000);
}

//var jsonGraph=[
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T13:53:00.000Z",
//    "arrivalTime": "2017-05-21T13:53:00.000Z",
//    "arrivalStop": "08892007",
//    "departureStop": "Gent-Sint-Pieters",
//    "@id": "1509815979"
//  },
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T13:53:00.000Z",
//    "arrivalTime": "2017-05-21T14:02:00.000Z",
//    "arrivalStop": "08893120",
//    "departureStop": "08892007",
//    "@id": "1081728023"
//  },
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T14:02:00.000Z",
//    "arrivalTime": "2017-05-21T14:02:00.000Z",
//    "arrivalStop": "Gent-Dampoort",
//    "departureStop": "08893120",
//    "@id": "1970525545"
//  },
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T14:02:00.000Z",
//    "arrivalTime": "2017-05-21T14:03:52.052Z",
//    "arrivalStop": "Gent Dampoort perron 12",
//    "departureStop": "Gent-Dampoort",
//    "@id": "1482751264"
//  },
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T15:00:00.000Z",
//    "arrivalTime": "2017-05-21T15:20:00.000Z",
//    "arrivalStop": "Beervelde Bennesteeg",
//    "departureStop": "Gent Dampoort perron 12",
//    "@id": "2127543807"
//  },
//  {
//    "@type": "Connection",
//    "departureTime": "2017-05-21T15:20:00.000Z",
//    "arrivalTime": "2017-05-21T15:30:08.008Z",
//    "arrivalStop": "Beerveldsebaan 19-21, 9080 Lochristi, Belgium",
//    "departureStop": "Beervelde Bennesteeg",
//    "@id": "2000985068"
//  }
//];


function getLC(callback){
    console.log("even geduld aub");
    
    requestMod.post(
        'http://localhost:3000/endpoint/personalisatie',
        { 
            json: {
                "route": {
                    "from":"Station Gent-Sint-Pieters",
                    "to":"Station Lokeren"
                },
                "person":{
                    "id" : "user1",
                    "firstName" : "John",
                    "lastName" : "Doe",
                    "profileModules" : {
                        "personal" : {
                            "preferredLanguage" : "EN",
                            "sex" : "M",
                            "age" : 30,
                            "walkingspeed":"normal"
                        },
                        "transport" : {
                            "maxWalkingDistance" : 2000,
                            "transferTimeNeeded" : 300,
                            "routeSortingPreference" : "minimizeDuration",
                            "modeRanking" : [
                                {
                                  "transportMode" : "train",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "bus",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "metro",
                                  "score" : 1
                                },
                                {
                                  "transportMode" : "tram",
                                  "score" : 1 
                                },
                                {
                                  "transportMode" : "foot",
                                  "score" : 1 
                                }

                            ]

                        }
                    }
                }
            } 
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
                callback(body);
            }
        }
    );
}
